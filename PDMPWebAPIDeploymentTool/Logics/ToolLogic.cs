﻿using PDMPWebAPIDeploymentTool.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDMPWebAPIDeploymentTool.Logics
{
    public class ToolLogic
    {
        private static readonly string NODE_SPECIFIC_SETTINGS_FILE_NAME = "NodeSpecificSettings.txt";
        public string ShowFolderBrowserDialog()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }
            else
            {
                return "";
            }
        }

        public ToolSettings ReadToolSettings()
        {
            ToolSettings toolSettings = new ToolSettings();
            Type type = typeof(ToolSettings);
            string propertyName;
            string propertyValue;
            foreach (PropertyInfo property in type.GetProperties())
            {
                propertyName = property.Name;
                propertyValue = ConfigurationManager.AppSettings[propertyName];
                property.SetValue(toolSettings, propertyValue);
            }
            return toolSettings;
        }

        public void WriteToolSettings(ToolSettings toolSettings)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            Type type = typeof(ToolSettings);
            string propertyName;
            string propertyValue;
            foreach (PropertyInfo property in type.GetProperties())
            {
                propertyName = property.Name;
                propertyValue = property.GetValue(toolSettings)?.ToString();
                if (config.AppSettings.Settings.AllKeys.Any(k => k == propertyName))
                {
                    config.AppSettings.Settings[propertyName].Value = propertyValue;
                }
                else
                {
                    config.AppSettings.Settings.Add(propertyName, propertyValue);
                }
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        public List<NodeSpecificSetting> ReadNodeSpecificSettings()
        {
            List<NodeSpecificSetting> nodeSpecificSettings = new List<NodeSpecificSetting>();
            NodeSpecificSetting nodeSpecificSetting = null;

            if (File.Exists(NODE_SPECIFIC_SETTINGS_FILE_NAME))
            {
                string[] allLines = File.ReadAllLines(NODE_SPECIFIC_SETTINGS_FILE_NAME);
                foreach (var line in allLines)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    nodeSpecificSetting = NodeSpecificSetting.FromJson(line);
                    if (nodeSpecificSetting != null)
                    {
                        nodeSpecificSettings.Add(nodeSpecificSetting);
                    }
                }
            }

            return nodeSpecificSettings;
        }

        public void WriteNodeSpecificSettings(List<NodeSpecificSetting> nodeSpecificSettings)
        {
            if (nodeSpecificSettings == null || nodeSpecificSettings.Count == 0)
            {
                return;
            }

            List<string> jsonStrings = new List<string>();
            string jsonString = "";

            foreach (var nodeSpecificSetting in nodeSpecificSettings)
            {
                jsonString = NodeSpecificSetting.ToJson(nodeSpecificSetting);
                if (!String.IsNullOrEmpty(jsonString))
                {
                    jsonStrings.Add(jsonString);
                }
            }

            if (jsonStrings.Count == 0)
            {
                return;
            }

            try
            {
                File.WriteAllText(NODE_SPECIFIC_SETTINGS_FILE_NAME, String.Join("\r\n", jsonStrings));
            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.AppendText("Log.txt"))
                {
                    sw.WriteLine($"Error writing the string below to {NODE_SPECIFIC_SETTINGS_FILE_NAME}:");
                    sw.WriteLine($"{String.Join("\r\n", jsonStrings)}");
                    sw.WriteLine("Exception:");
                    sw.WriteLine($"{e.ToString()}");
                    sw.WriteLine("==================================================");
                }
            }
        }

        public void AddNodeSetting(NodeSpecificSetting nodeSpecificSetting, BindingSource bindingSource)
        {
            if (String.IsNullOrEmpty(nodeSpecificSetting.NodeName) || String.IsNullOrEmpty(nodeSpecificSetting.NodeURL) || String.IsNullOrEmpty(nodeSpecificSetting.ConnecionString) || String.IsNullOrEmpty(nodeSpecificSetting.AdsSyncFrameworkExePath) || String.IsNullOrEmpty(nodeSpecificSetting.AdsSyncFrameworkRegion) || String.IsNullOrEmpty(nodeSpecificSetting.SGMailUtilExePath))
            {
                MessageBox.Show("Please fill in all the fields", "Adding node setting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            List<NodeSpecificSetting> nodeSpecificSettings = GetListOfNodeSettingsFromBindingSource(bindingSource);
            if (nodeSpecificSettings.Any(s => s.NodeName.ToLower() == nodeSpecificSetting.NodeName.ToLower()))
            {
                MessageBox.Show("The node already exists in the list", "Adding node setting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            bindingSource.Add(nodeSpecificSetting);
        }

        public void SaveFormInformation(ToolSettings toolSettings, BindingSource bindingSource)
        {
            if (String.IsNullOrEmpty(toolSettings.DeploymentPath) || String.IsNullOrEmpty(toolSettings.PDMPWebApiSourcePath) || String.IsNullOrEmpty(toolSettings.AdsSyncFrameworkSourcePath) || String.IsNullOrEmpty(toolSettings.SGMailUtilSourcePath))
            {
                MessageBox.Show("Please fill in all the tool settings", "Saving tool settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            WriteToolSettings(toolSettings);

            List<NodeSpecificSetting> nodeSpecificSettings = GetListOfNodeSettingsFromBindingSource(bindingSource);

            WriteNodeSpecificSettings(nodeSpecificSettings);

            MessageBox.Show("Saved successfully.", "Saving form information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public List<NodeSpecificSetting> GetListOfNodeSettingsFromBindingSource(BindingSource bindingSource)
        {
            NodeSpecificSetting[] nodeSpecificSettingsArray = new NodeSpecificSetting[bindingSource.Count];
            bindingSource.CopyTo(nodeSpecificSettingsArray, 0);
            List<NodeSpecificSetting> nodeSpecificSettings = nodeSpecificSettingsArray.ToList();
            return nodeSpecificSettings;
        }
    }
}
