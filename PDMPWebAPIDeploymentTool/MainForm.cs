﻿using PDMPWebAPIDeploymentTool.Logics;
using PDMPWebAPIDeploymentTool.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDMPWebAPIDeploymentTool
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void deploymentPathButton_Click(object sender, EventArgs e)
        {
            ToolLogic logic = new ToolLogic();

            string selectedPath = logic.ShowFolderBrowserDialog();
            if (!String.IsNullOrEmpty(selectedPath))
            {
                deploymentPathTextBox.Text = selectedPath; 
            }
        }

        private void pdmpWebAPISourcePathButton_Click(object sender, EventArgs e)
        {
            ToolLogic logic = new ToolLogic();

            string selectedPath = logic.ShowFolderBrowserDialog();
            if (!String.IsNullOrEmpty(selectedPath))
            {
                pdmpWebAPISourcePathTextBox.Text = selectedPath; 
            }
        }

        private void adsSyncFrameworkSourcePathButton_Click(object sender, EventArgs e)
        {
            ToolLogic logic = new ToolLogic();

            string selectedPath = logic.ShowFolderBrowserDialog();
            if (!String.IsNullOrEmpty(selectedPath))
            {
                adsSyncFrameworkSourcePathTextBox.Text = selectedPath; 
            }
        }

        private void sgMailUtilSourcePathButton_Click(object sender, EventArgs e)
        {
            ToolLogic logic = new ToolLogic();

            string selectedPath = logic.ShowFolderBrowserDialog();
            if (!String.IsNullOrEmpty(selectedPath))
            {
                sgMailUtilSourcePathTextBox.Text = selectedPath; 
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ToolLogic logic = new ToolLogic();

            ToolSettings toolSettings = logic.ReadToolSettings();
            ToToolSettingForm(toolSettings);

            List<NodeSpecificSetting> nodeSpecificSettings = logic.ReadNodeSpecificSettings();

            InitializeDataGridViewBindingSource(nodeSpecificSettings);
        }

        private void InitializeDataGridViewBindingSource(List<NodeSpecificSetting> nodeSpecificSettings)
        {
            foreach (var setting in nodeSpecificSettings)
            {
                dataGridViewBindingSource.Add(setting);
            }
            //dataGridViewBindingSource.DataSource = nodeSpecificSettings;

            nodeSettingsDataGridView.AutoGenerateColumns = true;
            nodeSettingsDataGridView.AutoSize = false;
            nodeSettingsDataGridView.DataSource = dataGridViewBindingSource;
            nodeSettingsDataGridView.AllowUserToAddRows = false;
            foreach (DataGridViewColumn column in nodeSettingsDataGridView.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void addNodeSettingButton_Click(object sender, EventArgs e)
        {
            NodeSpecificSetting nodeSpecificSetting = FromNodeSettingForm();

            ToolLogic logic = new ToolLogic();
            logic.AddNodeSetting(nodeSpecificSetting, dataGridViewBindingSource);

            ResetNodeSettingForm();
        }

        private void ResetNodeSettingForm()
        {
            nodeNameTextBox.Text = "";
            nodeUrlTextBox.Text = "";
            connectionStringTextBox.Text = "";
            transmitConnectionStringTextBox.Text = "";
            adsSyncFrameworkExePathTextBox.Text = "";
            sgMailUtilExePathTextBox.Text = "";
            azureServiceBusConnectionStringTextBox.Text = "";
            maintenanceSenderEmailTextBox.Text = "";
            maintenanceReceiverEmailTextBox.Text = "";

            adsSyncframeworkRegionTextBox.Text = "";
            adsUsernameTextBox.Text = "";
            adsPasswordTextBox.Text = "";
            adsAuthorityTextBox.Text = "";
            adsResourceTextBox.Text = "";
            adsClientIdTextBox.Text = "";
            adsBaseUrlTextBox.Text = "";
            adsSubscriptionHeaderTextBox.Text = "";
            adsSubscriptionValueTextBox.Text = "";

            nodeStartupCheckPathTextBox.Text = "";
        }

        private void saveSettingsButton_Click(object sender, EventArgs e)
        {
            ToolSettings toolSettings = FromToolSettingForm();

            ToolLogic logic = new ToolLogic();
            logic.SaveFormInformation(toolSettings, dataGridViewBindingSource);
        }

        private NodeSpecificSetting FromNodeSettingForm()
        {
            NodeSpecificSetting nodeSpecificSetting = new NodeSpecificSetting()
            {
                NodeName = nodeNameTextBox.Text,
                NodeURL = nodeUrlTextBox.Text,
                ConnecionString = connectionStringTextBox.Text,
                TransmitConnectionString = transmitConnectionStringTextBox.Text,
                AdsSyncFrameworkExePath = adsSyncFrameworkExePathTextBox.Text,
                SGMailUtilExePath = sgMailUtilExePathTextBox.Text,
                AzureServiceBusConnectionString = azureServiceBusConnectionStringTextBox.Text,
                MaintenanceSenderEmailAddress = maintenanceSenderEmailTextBox.Text,
                MaintenanceReceiverEmailAddress = maintenanceReceiverEmailTextBox.Text,
                AdsSyncFrameworkRegion = adsSyncframeworkRegionTextBox.Text,
                AdsUsername = adsUsernameTextBox.Text,
                AdsPassword = adsPasswordTextBox.Text,
                AdsAuthority = adsAuthorityTextBox.Text,
                AdsResource = adsResourceTextBox.Text,
                AdsClientId = adsClientIdTextBox.Text,
                AdsBaseUrl = adsBaseUrlTextBox.Text,
                AdsSubscriptionHeader = adsSubscriptionHeaderTextBox.Text,
                AdsSubscriptionValue = adsSubscriptionValueTextBox.Text,
                NodeStartupCheckPath = nodeStartupCheckPathTextBox.Text
            };

            return nodeSpecificSetting;
        }

        private void ToToolSettingForm(ToolSettings toolSettings)
        {
            deploymentPathTextBox.Text = toolSettings.DeploymentPath;
            pdmpWebAPISourcePathTextBox.Text = toolSettings.PDMPWebApiSourcePath;
            adsSyncFrameworkSourcePathTextBox.Text = toolSettings.AdsSyncFrameworkSourcePath;
            sgMailUtilSourcePathTextBox.Text = toolSettings.SGMailUtilSourcePath;
        }

        private ToolSettings FromToolSettingForm()
        {
            ToolSettings toolSettings = new ToolSettings()
            {
                DeploymentPath = deploymentPathTextBox.Text,
                PDMPWebApiSourcePath = pdmpWebAPISourcePathTextBox.Text,
                AdsSyncFrameworkSourcePath = adsSyncFrameworkSourcePathTextBox.Text,
                SGMailUtilSourcePath = sgMailUtilSourcePathTextBox.Text
            };

            return toolSettings;
        }

        private void makeDeploymentPackageButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            ToolSettings toolSettings = FromToolSettingForm();
            List<NodeSpecificSetting> nodeSpecificSettings = (new ToolLogic()).GetListOfNodeSettingsFromBindingSource(dataGridViewBindingSource);

            (new PackagingLogic()).MakeDeployementPackage(toolSettings, nodeSpecificSettings);

            this.Enabled = true;
        }
    }
}
