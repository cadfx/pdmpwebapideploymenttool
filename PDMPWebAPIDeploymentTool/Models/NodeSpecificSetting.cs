﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDMPWebAPIDeploymentTool.Models
{
    public class NodeSpecificSetting
    {
        #region Web API
        public string NodeName { get; set; }
        public string ConnecionString { get; set; }
        public string TransmitConnectionString { get; set; }
        public string NodeURL { get; set; }
        public string AdsSyncFrameworkExePath { get; set; }
        public string SGMailUtilExePath { get; set; }
        public string AzureServiceBusConnectionString { get; set; }
        public string MaintenanceSenderEmailAddress { get; set; }
        public string MaintenanceReceiverEmailAddress { get; set; }
        #endregion

        #region AdsSyncFramework
        public string AdsSyncFrameworkRegion { get; set; }
        public string AdsUsername { get; set; }
        public string AdsPassword { get; set; }
        public string AdsAuthority { get; set; }
        public string AdsResource { get; set; }
        public string AdsClientId { get; set; }
        public string AdsBaseUrl { get; set; }
        public string AdsSubscriptionHeader { get; set; }
        public string AdsSubscriptionValue { get; set; }
        #endregion

        public string NodeStartupCheckPath { get; set; }

        public static string ToJson(NodeSpecificSetting nodeSpecificSetting)
        {
            return JsonConvert.SerializeObject(nodeSpecificSetting);
        }

        public static NodeSpecificSetting FromJson(string jsonString)
        {
            NodeSpecificSetting nodeSpecificSetting = null;
            try
            {
                nodeSpecificSetting = JsonConvert.DeserializeObject<NodeSpecificSetting>(jsonString);
            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.AppendText("Log.txt"))
                {
                    sw.WriteLine("Error deserializing the string:");
                    sw.WriteLine($"{jsonString}");
                    sw.WriteLine("Exception:");
                    sw.WriteLine($"{e.ToString()}");
                    sw.WriteLine("==================================================");
                }
                nodeSpecificSetting = null;
            }
            return nodeSpecificSetting;
        }
    }
}
