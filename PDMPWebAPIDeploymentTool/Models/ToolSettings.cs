﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDMPWebAPIDeploymentTool.Models
{
    public class ToolSettings
    {
        public string DeploymentPath { get; set; }
        public string PDMPWebApiSourcePath { get; set; }
        public string AdsSyncFrameworkSourcePath { get; set; }
        public string SGMailUtilSourcePath { get; set; }
    }
}
