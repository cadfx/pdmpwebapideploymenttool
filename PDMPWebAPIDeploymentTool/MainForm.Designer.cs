﻿namespace PDMPWebAPIDeploymentTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sgMailUtilSourcePathButton = new System.Windows.Forms.Button();
            this.sgMailUtilSourcePathTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.adsSyncFrameworkSourcePathButton = new System.Windows.Forms.Button();
            this.adsSyncFrameworkSourcePathTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pdmpWebAPISourcePathButton = new System.Windows.Forms.Button();
            this.pdmpWebAPISourcePathTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.deploymentPathButton = new System.Windows.Forms.Button();
            this.deploymentPathTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nodeSettingsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.nodeStartupCheckPathTextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.adsSubscriptionHeaderTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.adsSubscriptionValueTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.adsClientIdTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.adsBaseUrlTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.adsAuthorityTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.adsResourceTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.adsUsernameTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.adsPasswordTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.maintenanceSenderEmailTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.maintenanceReceiverEmailTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.azureServiceBusConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.adsSyncframeworkRegionTextBox = new System.Windows.Forms.TextBox();
            this.addNodeSettingButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.adsSyncFrameworkExePathTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.sgMailUtilExePathTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.connectionStringTextBox = new System.Windows.Forms.TextBox();
            this.nodeUrlTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nodeNameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.saveSettingsButton = new System.Windows.Forms.Button();
            this.makeDeploymentPackageButton = new System.Windows.Forms.Button();
            this.transmitConnectionStringTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nodeSettingsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sgMailUtilSourcePathButton);
            this.groupBox1.Controls.Add(this.sgMailUtilSourcePathTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.adsSyncFrameworkSourcePathButton);
            this.groupBox1.Controls.Add(this.adsSyncFrameworkSourcePathTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.pdmpWebAPISourcePathButton);
            this.groupBox1.Controls.Add(this.pdmpWebAPISourcePathTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.deploymentPathButton);
            this.groupBox1.Controls.Add(this.deploymentPathTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 188);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tool specific settings";
            // 
            // sgMailUtilSourcePathButton
            // 
            this.sgMailUtilSourcePathButton.Location = new System.Drawing.Point(695, 147);
            this.sgMailUtilSourcePathButton.Name = "sgMailUtilSourcePathButton";
            this.sgMailUtilSourcePathButton.Size = new System.Drawing.Size(75, 23);
            this.sgMailUtilSourcePathButton.TabIndex = 11;
            this.sgMailUtilSourcePathButton.Text = "Browse";
            this.sgMailUtilSourcePathButton.UseVisualStyleBackColor = true;
            this.sgMailUtilSourcePathButton.Click += new System.EventHandler(this.sgMailUtilSourcePathButton_Click);
            // 
            // sgMailUtilSourcePathTextBox
            // 
            this.sgMailUtilSourcePathTextBox.Location = new System.Drawing.Point(6, 149);
            this.sgMailUtilSourcePathTextBox.Name = "sgMailUtilSourcePathTextBox";
            this.sgMailUtilSourcePathTextBox.Size = new System.Drawing.Size(683, 20);
            this.sgMailUtilSourcePathTextBox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "SGMail util source path:";
            // 
            // adsSyncFrameworkSourcePathButton
            // 
            this.adsSyncFrameworkSourcePathButton.Location = new System.Drawing.Point(695, 108);
            this.adsSyncFrameworkSourcePathButton.Name = "adsSyncFrameworkSourcePathButton";
            this.adsSyncFrameworkSourcePathButton.Size = new System.Drawing.Size(75, 23);
            this.adsSyncFrameworkSourcePathButton.TabIndex = 8;
            this.adsSyncFrameworkSourcePathButton.Text = "Browse";
            this.adsSyncFrameworkSourcePathButton.UseVisualStyleBackColor = true;
            this.adsSyncFrameworkSourcePathButton.Click += new System.EventHandler(this.adsSyncFrameworkSourcePathButton_Click);
            // 
            // adsSyncFrameworkSourcePathTextBox
            // 
            this.adsSyncFrameworkSourcePathTextBox.Location = new System.Drawing.Point(6, 110);
            this.adsSyncFrameworkSourcePathTextBox.Name = "adsSyncFrameworkSourcePathTextBox";
            this.adsSyncFrameworkSourcePathTextBox.Size = new System.Drawing.Size(683, 20);
            this.adsSyncFrameworkSourcePathTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "ADS-Sync-Framework source path:";
            // 
            // pdmpWebAPISourcePathButton
            // 
            this.pdmpWebAPISourcePathButton.Location = new System.Drawing.Point(695, 69);
            this.pdmpWebAPISourcePathButton.Name = "pdmpWebAPISourcePathButton";
            this.pdmpWebAPISourcePathButton.Size = new System.Drawing.Size(75, 23);
            this.pdmpWebAPISourcePathButton.TabIndex = 5;
            this.pdmpWebAPISourcePathButton.Text = "Browse";
            this.pdmpWebAPISourcePathButton.UseVisualStyleBackColor = true;
            this.pdmpWebAPISourcePathButton.Click += new System.EventHandler(this.pdmpWebAPISourcePathButton_Click);
            // 
            // pdmpWebAPISourcePathTextBox
            // 
            this.pdmpWebAPISourcePathTextBox.Location = new System.Drawing.Point(6, 71);
            this.pdmpWebAPISourcePathTextBox.Name = "pdmpWebAPISourcePathTextBox";
            this.pdmpWebAPISourcePathTextBox.Size = new System.Drawing.Size(683, 20);
            this.pdmpWebAPISourcePathTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "PDMP web API source path:";
            // 
            // deploymentPathButton
            // 
            this.deploymentPathButton.Location = new System.Drawing.Point(695, 30);
            this.deploymentPathButton.Name = "deploymentPathButton";
            this.deploymentPathButton.Size = new System.Drawing.Size(75, 23);
            this.deploymentPathButton.TabIndex = 2;
            this.deploymentPathButton.Text = "Browse";
            this.deploymentPathButton.UseVisualStyleBackColor = true;
            this.deploymentPathButton.Click += new System.EventHandler(this.deploymentPathButton_Click);
            // 
            // deploymentPathTextBox
            // 
            this.deploymentPathTextBox.Location = new System.Drawing.Point(6, 32);
            this.deploymentPathTextBox.Name = "deploymentPathTextBox";
            this.deploymentPathTextBox.Size = new System.Drawing.Size(683, 20);
            this.deploymentPathTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Deployment path:";
            // 
            // nodeSettingsDataGridView
            // 
            this.nodeSettingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.nodeSettingsDataGridView.Location = new System.Drawing.Point(12, 206);
            this.nodeSettingsDataGridView.Name = "nodeSettingsDataGridView";
            this.nodeSettingsDataGridView.Size = new System.Drawing.Size(776, 189);
            this.nodeSettingsDataGridView.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.transmitConnectionStringTextBox);
            this.groupBox2.Controls.Add(this.nodeStartupCheckPathTextBox);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.adsSubscriptionHeaderTextBox);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.adsSubscriptionValueTextBox);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.adsClientIdTextBox);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.adsBaseUrlTextBox);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.adsAuthorityTextBox);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.adsResourceTextBox);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.adsUsernameTextBox);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.adsPasswordTextBox);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.maintenanceSenderEmailTextBox);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.maintenanceReceiverEmailTextBox);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.azureServiceBusConnectionStringTextBox);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.adsSyncframeworkRegionTextBox);
            this.groupBox2.Controls.Add(this.addNodeSettingButton);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.adsSyncFrameworkExePathTextBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.sgMailUtilExePathTextBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.connectionStringTextBox);
            this.groupBox2.Controls.Add(this.nodeUrlTextBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.nodeNameTextBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 401);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(776, 355);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Node setting";
            // 
            // nodeStartupCheckPathTextBox
            // 
            this.nodeStartupCheckPathTextBox.Location = new System.Drawing.Point(187, 195);
            this.nodeStartupCheckPathTextBox.Name = "nodeStartupCheckPathTextBox";
            this.nodeStartupCheckPathTextBox.Size = new System.Drawing.Size(583, 20);
            this.nodeStartupCheckPathTextBox.TabIndex = 36;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 198);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(128, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "Node startup check path:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 302);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(127, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "ADS subscription header:";
            // 
            // adsSubscriptionHeaderTextBox
            // 
            this.adsSubscriptionHeaderTextBox.Location = new System.Drawing.Point(187, 299);
            this.adsSubscriptionHeaderTextBox.Name = "adsSubscriptionHeaderTextBox";
            this.adsSubscriptionHeaderTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsSubscriptionHeaderTextBox.TabIndex = 33;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(424, 302);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(120, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "ADS subscription value:";
            // 
            // adsSubscriptionValueTextBox
            // 
            this.adsSubscriptionValueTextBox.Location = new System.Drawing.Point(570, 299);
            this.adsSubscriptionValueTextBox.Name = "adsSubscriptionValueTextBox";
            this.adsSubscriptionValueTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsSubscriptionValueTextBox.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 276);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "ADS client ID:";
            // 
            // adsClientIdTextBox
            // 
            this.adsClientIdTextBox.Location = new System.Drawing.Point(187, 273);
            this.adsClientIdTextBox.Name = "adsClientIdTextBox";
            this.adsClientIdTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsClientIdTextBox.TabIndex = 29;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(424, 276);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "ADS base URL:";
            // 
            // adsBaseUrlTextBox
            // 
            this.adsBaseUrlTextBox.Location = new System.Drawing.Point(570, 273);
            this.adsBaseUrlTextBox.Name = "adsBaseUrlTextBox";
            this.adsBaseUrlTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsBaseUrlTextBox.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 250);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "ADS authority:";
            // 
            // adsAuthorityTextBox
            // 
            this.adsAuthorityTextBox.Location = new System.Drawing.Point(187, 247);
            this.adsAuthorityTextBox.Name = "adsAuthorityTextBox";
            this.adsAuthorityTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsAuthorityTextBox.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(424, 250);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "ADS resource:";
            // 
            // adsResourceTextBox
            // 
            this.adsResourceTextBox.Location = new System.Drawing.Point(570, 247);
            this.adsResourceTextBox.Name = "adsResourceTextBox";
            this.adsResourceTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsResourceTextBox.TabIndex = 23;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 224);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "ADS username:";
            // 
            // adsUsernameTextBox
            // 
            this.adsUsernameTextBox.Location = new System.Drawing.Point(187, 221);
            this.adsUsernameTextBox.Name = "adsUsernameTextBox";
            this.adsUsernameTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsUsernameTextBox.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(424, 224);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "ADS password (B64):";
            // 
            // adsPasswordTextBox
            // 
            this.adsPasswordTextBox.Location = new System.Drawing.Point(570, 221);
            this.adsPasswordTextBox.Name = "adsPasswordTextBox";
            this.adsPasswordTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsPasswordTextBox.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 172);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "Maintenance sender email:";
            // 
            // maintenanceSenderEmailTextBox
            // 
            this.maintenanceSenderEmailTextBox.Location = new System.Drawing.Point(187, 169);
            this.maintenanceSenderEmailTextBox.Name = "maintenanceSenderEmailTextBox";
            this.maintenanceSenderEmailTextBox.Size = new System.Drawing.Size(200, 20);
            this.maintenanceSenderEmailTextBox.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(424, 172);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(140, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Maintenance receiver email:";
            // 
            // maintenanceReceiverEmailTextBox
            // 
            this.maintenanceReceiverEmailTextBox.Location = new System.Drawing.Point(570, 169);
            this.maintenanceReceiverEmailTextBox.Name = "maintenanceReceiverEmailTextBox";
            this.maintenanceReceiverEmailTextBox.Size = new System.Drawing.Size(200, 20);
            this.maintenanceReceiverEmailTextBox.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(175, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Azure sevice bus connection string:";
            // 
            // azureServiceBusConnectionStringTextBox
            // 
            this.azureServiceBusConnectionStringTextBox.Location = new System.Drawing.Point(187, 143);
            this.azureServiceBusConnectionStringTextBox.Name = "azureServiceBusConnectionStringTextBox";
            this.azureServiceBusConnectionStringTextBox.Size = new System.Drawing.Size(583, 20);
            this.azureServiceBusConnectionStringTextBox.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 328);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "ads-sync-framework region:";
            // 
            // adsSyncframeworkRegionTextBox
            // 
            this.adsSyncframeworkRegionTextBox.Location = new System.Drawing.Point(187, 325);
            this.adsSyncframeworkRegionTextBox.Name = "adsSyncframeworkRegionTextBox";
            this.adsSyncframeworkRegionTextBox.Size = new System.Drawing.Size(200, 20);
            this.adsSyncframeworkRegionTextBox.TabIndex = 11;
            // 
            // addNodeSettingButton
            // 
            this.addNodeSettingButton.Location = new System.Drawing.Point(695, 325);
            this.addNodeSettingButton.Name = "addNodeSettingButton";
            this.addNodeSettingButton.Size = new System.Drawing.Size(75, 23);
            this.addNodeSettingButton.TabIndex = 10;
            this.addNodeSettingButton.Text = "Add";
            this.addNodeSettingButton.UseVisualStyleBackColor = true;
            this.addNodeSettingButton.Click += new System.EventHandler(this.addNodeSettingButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "ads-sync-framework path:";
            // 
            // adsSyncFrameworkExePathTextBox
            // 
            this.adsSyncFrameworkExePathTextBox.Location = new System.Drawing.Point(187, 117);
            this.adsSyncFrameworkExePathTextBox.Name = "adsSyncFrameworkExePathTextBox";
            this.adsSyncFrameworkExePathTextBox.Size = new System.Drawing.Size(583, 20);
            this.adsSyncFrameworkExePathTextBox.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "SGMailUtil path:";
            // 
            // sgMailUtilExePathTextBox
            // 
            this.sgMailUtilExePathTextBox.Location = new System.Drawing.Point(187, 91);
            this.sgMailUtilExePathTextBox.Name = "sgMailUtilExePathTextBox";
            this.sgMailUtilExePathTextBox.Size = new System.Drawing.Size(583, 20);
            this.sgMailUtilExePathTextBox.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "ProjectData connection string:";
            // 
            // connectionStringTextBox
            // 
            this.connectionStringTextBox.Location = new System.Drawing.Point(187, 39);
            this.connectionStringTextBox.Name = "connectionStringTextBox";
            this.connectionStringTextBox.Size = new System.Drawing.Size(583, 20);
            this.connectionStringTextBox.TabIndex = 4;
            // 
            // nodeUrlTextBox
            // 
            this.nodeUrlTextBox.Location = new System.Drawing.Point(570, 13);
            this.nodeUrlTextBox.Name = "nodeUrlTextBox";
            this.nodeUrlTextBox.Size = new System.Drawing.Size(200, 20);
            this.nodeUrlTextBox.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(424, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Node URL:";
            // 
            // nodeNameTextBox
            // 
            this.nodeNameTextBox.Location = new System.Drawing.Point(187, 13);
            this.nodeNameTextBox.Name = "nodeNameTextBox";
            this.nodeNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.nodeNameTextBox.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Node name:";
            // 
            // saveSettingsButton
            // 
            this.saveSettingsButton.Location = new System.Drawing.Point(707, 762);
            this.saveSettingsButton.Name = "saveSettingsButton";
            this.saveSettingsButton.Size = new System.Drawing.Size(75, 23);
            this.saveSettingsButton.TabIndex = 3;
            this.saveSettingsButton.Text = "Save";
            this.saveSettingsButton.UseVisualStyleBackColor = true;
            this.saveSettingsButton.Click += new System.EventHandler(this.saveSettingsButton_Click);
            // 
            // makeDeploymentPackageButton
            // 
            this.makeDeploymentPackageButton.Location = new System.Drawing.Point(18, 762);
            this.makeDeploymentPackageButton.Name = "makeDeploymentPackageButton";
            this.makeDeploymentPackageButton.Size = new System.Drawing.Size(173, 23);
            this.makeDeploymentPackageButton.TabIndex = 4;
            this.makeDeploymentPackageButton.Text = "Make Deployment Package";
            this.makeDeploymentPackageButton.UseVisualStyleBackColor = true;
            this.makeDeploymentPackageButton.Click += new System.EventHandler(this.makeDeploymentPackageButton_Click);
            // 
            // transmitConnectionStringTextBox
            // 
            this.transmitConnectionStringTextBox.Location = new System.Drawing.Point(187, 65);
            this.transmitConnectionStringTextBox.Name = "transmitConnectionStringTextBox";
            this.transmitConnectionStringTextBox.Size = new System.Drawing.Size(583, 20);
            this.transmitConnectionStringTextBox.TabIndex = 37;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 68);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(134, 13);
            this.label23.TabIndex = 38;
            this.label23.Text = "Transmit connection string:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 797);
            this.Controls.Add(this.makeDeploymentPackageButton);
            this.Controls.Add(this.saveSettingsButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.nodeSettingsDataGridView);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "PDMP WebAPI Deployment Tool";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nodeSettingsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button sgMailUtilSourcePathButton;
        private System.Windows.Forms.TextBox sgMailUtilSourcePathTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button adsSyncFrameworkSourcePathButton;
        private System.Windows.Forms.TextBox adsSyncFrameworkSourcePathTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button pdmpWebAPISourcePathButton;
        private System.Windows.Forms.TextBox pdmpWebAPISourcePathTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button deploymentPathButton;
        private System.Windows.Forms.TextBox deploymentPathTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView nodeSettingsDataGridView;
        private System.Windows.Forms.BindingSource dataGridViewBindingSource;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox connectionStringTextBox;
        private System.Windows.Forms.TextBox nodeUrlTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox nodeNameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button addNodeSettingButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox adsSyncFrameworkExePathTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox sgMailUtilExePathTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox adsSyncframeworkRegionTextBox;
        private System.Windows.Forms.Button saveSettingsButton;
        private System.Windows.Forms.Button makeDeploymentPackageButton;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox adsSubscriptionHeaderTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox adsSubscriptionValueTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox adsClientIdTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox adsBaseUrlTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox adsAuthorityTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox adsResourceTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox adsUsernameTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox adsPasswordTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox maintenanceSenderEmailTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox maintenanceReceiverEmailTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox azureServiceBusConnectionStringTextBox;
        private System.Windows.Forms.TextBox nodeStartupCheckPathTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox transmitConnectionStringTextBox;
    }
}

