﻿using PDEDeploymentTool.Logic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace PDEDeploymentTool.Model
{
    public class SharedDataModel : INotifyPropertyChanged
    {
        public ToolSettings ToolSettings { get; set; }
        public ObservableCollection<NodeSpecificSetting> NodeSpecificSettings { get; set; }

        private NodeSpecificSetting currentNodeSpecificSetting;
        public NodeSpecificSetting CurrentNodeSpecificSetting
        {
            get { return currentNodeSpecificSetting; }
            set
            {
                if (value != currentNodeSpecificSetting)
                {
                    currentNodeSpecificSetting = value;
                    NotifyPropertyChange();
                }
            }
        }

        public SharedDataModel()
        {
            try
            {
                ToolSettings = ToolHelper.ReadToolSettings();
                currentNodeSpecificSetting = new NodeSpecificSetting();
                NodeSpecificSettings = new ObservableCollection<NodeSpecificSetting>(ToolHelper.ReadNodeSpecificSettings());
            }
            catch (System.Exception e)
            {
                MessageBox.Show($"Error reading the configurations!\r\n{e}", "Initialization Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChange([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
