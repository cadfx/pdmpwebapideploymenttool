﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PDEDeploymentTool.Model
{
    public class ToolSettings : INotifyPropertyChanged
    {
        private string deploymentPath;
        private string pdmpWebApiSourcePath;
        private string adsSyncFrameworkSourcePath;
        private string sGMailUtilSourcePath;
        private string changeManagementLogParserSourcePath;

        public string DeploymentPath
        {
            get { return deploymentPath; }
            set
            {
                if (value != deploymentPath)
                {
                    deploymentPath = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string PDMPWebApiSourcePath
        {
            get { return pdmpWebApiSourcePath; }
            set
            {
                if (value != pdmpWebApiSourcePath)
                {
                    pdmpWebApiSourcePath = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string AdsSyncFrameworkSourcePath
        {
            get { return adsSyncFrameworkSourcePath; }
            set
            {
                if (value != adsSyncFrameworkSourcePath)
                {
                    adsSyncFrameworkSourcePath = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string SGMailUtilSourcePath
        {
            get { return sGMailUtilSourcePath; }
            set
            {
                if (value != sGMailUtilSourcePath)
                {
                    sGMailUtilSourcePath = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string ChangeManagementLogParserSourcePath
        {
            get { return changeManagementLogParserSourcePath; }
            set
            {
                if (value != changeManagementLogParserSourcePath)
                {
                    changeManagementLogParserSourcePath = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
