﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text.Json;

namespace PDEDeploymentTool.Model
{
    public class NodeSpecificSetting : INotifyPropertyChanged
    {
        #region Web API
        private string nodeName;
        private string azureServiceBus_keyName;
        private string database_ConnectionString;
        private string database_TransmitConnectionString;
        private string clarity_BaseUrl;
        private string clarity_RestKeyName;
        private string nodeURL;
        private string startupCheck_NodeStartupPathCheck;
        private string maintenance_SenderEmailAddress;
        private string maintenance_ReceiverEmailAddress;
        private string changeManagement_ConnectionStringKeyName;
        private string changeManagement_BlobContainerName;
        private string transmitProjectBackup_ConnectionStringKeyName;
        private string transmitProjectBackup_BlobContainerName;
        private string transmitIssueLog_ConnectionStringKeyName;
        private string transmitIssueLog_BlobContainerName;

        public string NodeName
        {
            get { return nodeName; }
            set
            {
                if (value != nodeName)
                {
                    nodeName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string AzureServiceBus_keyName
        {
            get { return azureServiceBus_keyName; }
            set
            {
                if (value != azureServiceBus_keyName)
                {
                    azureServiceBus_keyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Database_ConnectionString
        {
            get { return database_ConnectionString; }
            set
            {
                if (value != database_ConnectionString)
                {
                    database_ConnectionString = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Database_TransmitConnectionString
        {
            get { return database_TransmitConnectionString; }
            set
            {
                if (value != database_TransmitConnectionString)
                {
                    database_TransmitConnectionString = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Clarity_BaseUrl
        {
            get { return clarity_BaseUrl; }
            set
            {
                if (value != clarity_BaseUrl)
                {
                    clarity_BaseUrl = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Clarity_RestKeyName
        {
            get { return clarity_RestKeyName; }
            set
            {
                if (value != clarity_RestKeyName)
                {
                    clarity_RestKeyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string NodeURL
        {
            get { return nodeURL; }
            set
            {
                if (value != nodeURL)
                {
                    nodeURL = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string StartupCheck_NodeStartupPathCheck
        {
            get { return startupCheck_NodeStartupPathCheck; }
            set
            {
                if (value != startupCheck_NodeStartupPathCheck)
                {
                    startupCheck_NodeStartupPathCheck = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Maintenance_SenderEmailAddress
        {
            get { return maintenance_SenderEmailAddress; }
            set
            {
                if (value != maintenance_SenderEmailAddress)
                {
                    maintenance_SenderEmailAddress = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string Maintenance_ReceiverEmailAddress
        {
            get { return maintenance_ReceiverEmailAddress; }
            set
            {
                if (value != maintenance_ReceiverEmailAddress)
                {
                    maintenance_ReceiverEmailAddress = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string ChangeManagement_ConnectionStringKeyName
        {
            get { return changeManagement_ConnectionStringKeyName; }
            set
            {
                if (value != changeManagement_ConnectionStringKeyName)
                {
                    changeManagement_ConnectionStringKeyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string ChangeManagement_BlobContainerName
        {
            get { return changeManagement_BlobContainerName; }
            set
            {
                if (value != changeManagement_BlobContainerName)
                {
                    changeManagement_BlobContainerName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string TransmitProjectBackup_ConnectionStringKeyName
        {
            get { return transmitProjectBackup_ConnectionStringKeyName; }
            set
            {
                if (value != transmitProjectBackup_ConnectionStringKeyName)
                {
                    transmitProjectBackup_ConnectionStringKeyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string TransmitProjectBackup_BlobContainerName
        {
            get { return transmitProjectBackup_BlobContainerName; }
            set
            {
                if (value != transmitProjectBackup_BlobContainerName)
                {
                    transmitProjectBackup_BlobContainerName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string TransmitIssueLog_ConnectionStringKeyName
        {
            get { return transmitIssueLog_ConnectionStringKeyName; }
            set
            {
                if (value != transmitIssueLog_ConnectionStringKeyName)
                {
                    transmitIssueLog_ConnectionStringKeyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string TransmitIssueLog_BlobContainerName
        {
            get { return transmitIssueLog_BlobContainerName; }
            set
            {
                if (value != transmitIssueLog_BlobContainerName)
                {
                    transmitIssueLog_BlobContainerName = value;
                    NotifyPropertyChange();
                }
            }
        }
        #endregion

        #region AdsSyncFramework
        private string adsSyncFrameworkRegion;
        private string adsBaseUrl;
        private string adsSubscriptionValue;
        private string keyVault;
        private string keyVaultClientId;
        private string keyVaultTenantId;
        private string adsKeyVaultKeyName;
        private string windowsCredentialClientSecret;
        private string otherNodes;

        public string AdsSyncFrameworkRegion
        {
            get { return adsSyncFrameworkRegion; }
            set
            {
                if (value != adsSyncFrameworkRegion)
                {
                    adsSyncFrameworkRegion = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string AdsBaseUrl
        {
            get { return adsBaseUrl; }
            set
            {
                if (value != adsBaseUrl)
                {
                    adsBaseUrl = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string AdsSubscriptionValue
        {
            get { return adsSubscriptionValue; }
            set
            {
                if (value != adsSubscriptionValue)
                {
                    adsSubscriptionValue = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string KeyVault
        {
            get { return keyVault; }
            set
            {
                if (value != keyVault)
                {
                    keyVault = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string KeyVaultClientId
        {
            get { return keyVaultClientId; }
            set
            {
                if (value != keyVaultClientId)
                {
                    keyVaultClientId = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string KeyVaultTenantId
        {
            get { return keyVaultTenantId; }
            set
            {
                if (value != keyVaultTenantId)
                {
                    keyVaultTenantId = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string AdsKeyVaultKeyName
        {
            get { return adsKeyVaultKeyName; }
            set
            {
                if (value != adsKeyVaultKeyName)
                {
                    adsKeyVaultKeyName = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string WinCredClientSecret
        {
            get { return windowsCredentialClientSecret; }
            set
            {
                if (value != windowsCredentialClientSecret)
                {
                    windowsCredentialClientSecret = value;
                    NotifyPropertyChange();
                }
            }
        }

        public string OtherNodes
        {
            get { return otherNodes; }
            set
            {
                if (value != otherNodes)
                {
                    otherNodes = value;
                    NotifyPropertyChange();
                }
            }
        }
        #endregion

        #region ChangeManagementTools
        private string changeManagementToolsRegion;

        public string ChangeManagementToolsRegion
        {
            get { return changeManagementToolsRegion; }
            set
            {
                if (value != changeManagementToolsRegion)
                {
                    changeManagementToolsRegion = value;
                    NotifyPropertyChange();
                }
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChange([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static string ToJson(NodeSpecificSetting nodeSpecificSetting)
        {
            string jsonString = JsonSerializer.Serialize(nodeSpecificSetting);
            return jsonString;
        }

        public static NodeSpecificSetting FromJson(string jsonString)
        {
            NodeSpecificSetting nodeSpecificSetting = null;
            try
            {
                nodeSpecificSetting = JsonSerializer.Deserialize<NodeSpecificSetting>(jsonString);
            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.AppendText("Log.txt"))
                {
                    sw.WriteLine("Error deserializing the string:");
                    sw.WriteLine($"{jsonString}");
                    sw.WriteLine("Exception:");
                    sw.WriteLine($"{e}");
                    sw.WriteLine("==================================================");
                }
                nodeSpecificSetting = null;
            }
            return nodeSpecificSetting;
        }

        public NodeSpecificSetting GetAClone()
        {
            return new NodeSpecificSetting()
            {
                NodeName = NodeName,
                AzureServiceBus_keyName = AzureServiceBus_keyName,
                Database_ConnectionString = Database_ConnectionString,
                Database_TransmitConnectionString = Database_TransmitConnectionString,
                Clarity_BaseUrl = Clarity_BaseUrl,
                Clarity_RestKeyName = Clarity_RestKeyName,
                NodeURL = NodeURL,
                StartupCheck_NodeStartupPathCheck = StartupCheck_NodeStartupPathCheck,
                Maintenance_SenderEmailAddress = Maintenance_SenderEmailAddress,
                Maintenance_ReceiverEmailAddress = Maintenance_ReceiverEmailAddress,
                ChangeManagement_ConnectionStringKeyName = ChangeManagement_ConnectionStringKeyName,
                ChangeManagement_BlobContainerName = ChangeManagement_BlobContainerName,
                TransmitProjectBackup_ConnectionStringKeyName = TransmitProjectBackup_ConnectionStringKeyName,
                TransmitProjectBackup_BlobContainerName = TransmitProjectBackup_BlobContainerName,
                TransmitIssueLog_ConnectionStringKeyName = TransmitIssueLog_ConnectionStringKeyName,
                TransmitIssueLog_BlobContainerName = TransmitIssueLog_BlobContainerName,

                AdsSyncFrameworkRegion = AdsSyncFrameworkRegion,
                AdsBaseUrl = AdsBaseUrl,
                AdsSubscriptionValue = AdsSubscriptionValue,
                KeyVault = KeyVault,
                KeyVaultClientId = KeyVaultClientId,
                KeyVaultTenantId = KeyVaultTenantId,
                AdsKeyVaultKeyName = AdsKeyVaultKeyName,
                WinCredClientSecret = WinCredClientSecret,
                OtherNodes = OtherNodes,

                ChangeManagementToolsRegion = ChangeManagementToolsRegion
            };
        }
    }
}
