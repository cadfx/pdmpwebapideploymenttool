﻿using PDEDeploymentTool.Logic;
using PDEDeploymentTool.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PDEDeploymentTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static SharedDataModel sharedDataModel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            string selectedPath = ToolHelper.ShowFolderBrowserDialog();
            if (!String.IsNullOrEmpty(selectedPath))
            {
                var selectedButton = sender as Button;

                if (selectedButton == deploymentPathButton)
                {
                    sharedDataModel.ToolSettings.DeploymentPath = selectedPath;
                }
                if (selectedButton == webApiSourcePathPathButton)
                {
                    sharedDataModel.ToolSettings.PDMPWebApiSourcePath = selectedPath;
                }
                if (selectedButton == adsSyncFrameworkSourcePathPathButton)
                {
                    sharedDataModel.ToolSettings.AdsSyncFrameworkSourcePath = selectedPath;
                }
                if (selectedButton == sGMailUtilSourcePathPathButton)
                {
                    sharedDataModel.ToolSettings.SGMailUtilSourcePath = selectedPath;
                }
                if (selectedButton == changeManagementToolsPathPathButton)
                {
                    sharedDataModel.ToolSettings.ChangeManagementLogParserSourcePath = selectedPath;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sharedDataModel = new SharedDataModel();
            this.DataContext = sharedDataModel;
            newRadioButton.IsChecked = true;
        }

        private void deploymentConfigurationSaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel?.ToolSettings == null)
            {
                MessageBox.Show("Unable to read the tool configurations!", "Configuration Reading Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
            else
            {
                ToolHelper.WriteToolSettings(sharedDataModel.ToolSettings);
            }
        }

        private void configurationSaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel?.NodeSpecificSettings != null && sharedDataModel?.NodeSpecificSettings.Count > 0)
            {
                ToolHelper.WriteNodeSpecificSettings(sharedDataModel.NodeSpecificSettings.ToList());
            }
        }

        private void nodeNamesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sharedDataModel == null)
            {
                return;
            }

            var selectedItem = (NodeSpecificSetting)nodeNamesComboBox.SelectedItem;
            if (selectedItem != null)
            {
                sharedDataModel.CurrentNodeSpecificSetting = selectedItem;
            }
        }

        private void newRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel == null)
            {
                return;
            }

            var radioButton = sender as RadioButton;
            if (radioButton != null && radioButton.IsChecked.HasValue && radioButton.IsChecked.Value)
            {
                var selectedItem = (NodeSpecificSetting)nodeNamesComboBox.SelectedItem;
                if (selectedItem != null)
                {
                    sharedDataModel.CurrentNodeSpecificSetting = selectedItem.GetAClone();
                }
                else
                {
                    sharedDataModel.CurrentNodeSpecificSetting = new NodeSpecificSetting();
                }
            }
        }

        private void editRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel == null)
            {
                return;
            }

            var radioButton = sender as RadioButton;
            if (radioButton != null && radioButton.IsChecked.HasValue && radioButton.IsChecked.Value)
            {
                var selectedItem = (NodeSpecificSetting)nodeNamesComboBox.SelectedItem;
                if (selectedItem != null)
                {
                    sharedDataModel.CurrentNodeSpecificSetting = selectedItem;
                }
            }
        }

        private void addNewNodeConfigurationButton_Click(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel?.CurrentNodeSpecificSetting != null && sharedDataModel?.NodeSpecificSettings != null)
            {
                sharedDataModel.NodeSpecificSettings.Add(sharedDataModel.CurrentNodeSpecificSetting);
                sharedDataModel.CurrentNodeSpecificSetting = new NodeSpecificSetting();
                MessageBox.Show("Configuration got added to the list.", "Node Specific Configuration", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void deploymentButton_Click(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel?.ToolSettings != null && sharedDataModel?.NodeSpecificSettings != null && sharedDataModel?.NodeSpecificSettings.Count > 0)
            {
                DeploymentLogic deploymentLogic = new DeploymentLogic();
                deploymentLogic.MakeDeployementPackage(sharedDataModel.ToolSettings, sharedDataModel.NodeSpecificSettings.ToList());
            }
        }

        private void resetAllTextBoxesButton_Click(object sender, RoutedEventArgs e)
        {
            if (sharedDataModel != null)
            {
                sharedDataModel.CurrentNodeSpecificSetting = new NodeSpecificSetting();
            }
        }
    }
}
