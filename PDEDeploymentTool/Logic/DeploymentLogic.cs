﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PDEDeploymentTool.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Windows;

namespace PDEDeploymentTool.Logic
{
    public class DeploymentLogic
    {
        private static readonly string WEB_API_COMMON_FOLDER_NAME = "WebAPI";
        private static readonly string WEB_API_NODE_FOLDER_NAME = "WebAPI";
        private static readonly string ADS_SYNC_FRAMEWORK_COMMON_FOLDER_NAME = "ads-sync-framework";
        private static readonly string ADS_SYNC_FRAMEWORK_NODE_FOLDER_NAME = "ads-sync-framework";
        private static readonly string SG_MAIL_UTIL_COMMON_FOLDER_NAME = "SGMailUtil";
        private static readonly string CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME = "AzureBlobParser";

        public void MakeDeployementPackage(ToolSettings toolSettings, List<NodeSpecificSetting> nodeSpecificSettings)
        {
            string errMsg = Validate(toolSettings, nodeSpecificSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                MessageBox.Show($"Error:\r\n{errMsg}", "Validation", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            errMsg = CreateFolderStructures(toolSettings, nodeSpecificSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                MessageBox.Show($"Error creating folder structures:\r\n{errMsg}", "Package creation", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            errMsg = CopyCommonFiles(toolSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                MessageBox.Show($"Error copying common files:\r\n{errMsg}", "Package creation", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            errMsg = RemoveSpecificFilesFromCommonFolders(toolSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                MessageBox.Show($"Error deleting specific files from common folders:\r\n{errMsg}", "Deployment Result Result", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            errMsg = WriteNodeSpeificFiles(toolSettings, nodeSpecificSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                MessageBox.Show($"{errMsg}", "Deployment Result", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            MessageBox.Show("Package has been created successfully", "Deployment Result", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private string Validate(ToolSettings toolSettings, List<NodeSpecificSetting> nodeSpecificSettings)
        {
            string errMsg = "";

            errMsg = AreFoldersValid(toolSettings);
            if (!String.IsNullOrEmpty(errMsg))
            {
                return errMsg;
            }

            if (nodeSpecificSettings == null || nodeSpecificSettings.Count == 0)
            {
                return "List of nodes is empty";
            }

            return "";
        }

        private string AreFoldersValid(ToolSettings toolSettings)
        {
            if (!FolderExists(toolSettings.DeploymentPath))
            {
                return "Deployement path does not exist.";
            }

            if (!FolderExists(toolSettings.PDMPWebApiSourcePath))
            {
                return "PDMP web API source path does not exist.";
            }

            if (!FolderExists(toolSettings.AdsSyncFrameworkSourcePath))
            {
                return "ADS-Sync-Framework source path does not exist.";
            }

            if (!FolderExists(toolSettings.SGMailUtilSourcePath))
            {
                return "SGMail util source path does not exist.";
            }

            if (!FolderExists(toolSettings.ChangeManagementLogParserSourcePath))
            {
                return "Change management tools source path does not exist.";
            }

            return "";
        }

        private bool FolderExists(string path)
        {
            return Directory.Exists(path);
        }

        private string CreateFolderStructures(ToolSettings toolSettings, List<NodeSpecificSetting> nodeSpecificSettings)
        {
            string errMsg = "";
            try
            {
                Directory.CreateDirectory(toolSettings.DeploymentPath);
                Directory.CreateDirectory(Path.Combine(toolSettings.DeploymentPath, WEB_API_COMMON_FOLDER_NAME));
                Directory.CreateDirectory(Path.Combine(toolSettings.DeploymentPath, ADS_SYNC_FRAMEWORK_COMMON_FOLDER_NAME));
                Directory.CreateDirectory(Path.Combine(toolSettings.DeploymentPath, SG_MAIL_UTIL_COMMON_FOLDER_NAME));
                Directory.CreateDirectory(Path.Combine(toolSettings.DeploymentPath, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME));

                string nodesFolder = Path.Combine(toolSettings.DeploymentPath, "Nodes");
                string currentNodeFolder = "";
                Directory.CreateDirectory(nodesFolder);
                foreach (var nodeSetting in nodeSpecificSettings)
                {
                    currentNodeFolder = Path.Combine(nodesFolder, nodeSetting.NodeName);
                    Directory.CreateDirectory(Path.Combine(currentNodeFolder, WEB_API_NODE_FOLDER_NAME));
                    Directory.CreateDirectory(Path.Combine(currentNodeFolder, ADS_SYNC_FRAMEWORK_NODE_FOLDER_NAME));
                    Directory.CreateDirectory(Path.Combine(currentNodeFolder, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME));
                }
            }
            catch (Exception e)
            {
                errMsg = e.ToString();
            }
            return errMsg;
        }

        private string CopyCommonFiles(ToolSettings toolSettings)
        {
            string errMsg = "";

            errMsg = CopyFolderContent(toolSettings.PDMPWebApiSourcePath, Path.Combine(toolSettings.DeploymentPath, WEB_API_COMMON_FOLDER_NAME));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Copy failed for WebAPI folder.\r\n" + errMsg;
                return errMsg;
            }

            errMsg = CopyFolderContent(toolSettings.AdsSyncFrameworkSourcePath, Path.Combine(toolSettings.DeploymentPath, ADS_SYNC_FRAMEWORK_COMMON_FOLDER_NAME));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Copy failed for ads-sync-framework folder.\r\n" + errMsg;
                return errMsg;
            }

            errMsg = CopyFolderContent(toolSettings.SGMailUtilSourcePath, Path.Combine(toolSettings.DeploymentPath, SG_MAIL_UTIL_COMMON_FOLDER_NAME));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Copy failed for SGMailUtil folder.\r\n" + errMsg;
                return errMsg;
            }

            errMsg = CopyFolderContent(toolSettings.ChangeManagementLogParserSourcePath, Path.Combine(toolSettings.DeploymentPath, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Copy failed for AzureBlobParser folder.\r\n" + errMsg;
                return errMsg;
            }

            return errMsg;
        }

        private string CopyFolderContent(string source, string destination)
        {
            string errMsg = "";
            try
            {
                // Create all the directories.
                foreach (string directory in Directory.GetDirectories(source, "*", SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(directory.Replace(source, destination));
                }

                // Copy all the files & Replaces any files with the same name.
                foreach (string file in Directory.GetFiles(source, "*.*", SearchOption.AllDirectories))
                {
                    File.Copy(file, file.Replace(source, destination), true);
                }
            }
            catch (Exception e)
            {
                errMsg = e.ToString();
            }

            return errMsg;
        }

        private string RemoveSpecificFilesFromCommonFolders(ToolSettings toolSettings)
        {
            string errMsg = "";

            errMsg = DeleteFile(Path.Combine(toolSettings.DeploymentPath, WEB_API_COMMON_FOLDER_NAME, "appsettings.json"));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Error deleting \"appsettings.json\" from \"WebAPI\" folder:\r\n" + errMsg;
                return errMsg;
            }

            errMsg = DeleteFile(Path.Combine(toolSettings.DeploymentPath, ADS_SYNC_FRAMEWORK_COMMON_FOLDER_NAME, "ads-sync-framework.exe.config"));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Error deleting \"ads-sync-framework.exe.config\" from \"ads-sync-framework\" folder:\r\n" + errMsg;
                return errMsg;
            }

            errMsg = DeleteFile(Path.Combine(toolSettings.DeploymentPath, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME, "appsettings.json"));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Error deleting \"appsettings.json\" from \"AzureBlobParser\" folder:\r\n" + errMsg;
                return errMsg;
            }

            errMsg = DeleteFile(Path.Combine(toolSettings.DeploymentPath, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME, "AzureBlobParser.runtimeconfig.dev.json"));
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = "Error deleting \"AzureBlobParser.runtimeconfig.dev.json\" from \"AzureBlobParser\" folder:\r\n" + errMsg;
                return errMsg;
            }

            return "";
        }

        private string DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            return "";
        }

        private string WriteNodeSpeificFiles(ToolSettings toolSettings, List<NodeSpecificSetting> nodeSpecificSettings)
        {
            string errMsg = "";

            foreach (var node in nodeSpecificSettings)
            {
                errMsg = WriteNodeSpecificFilesForSpecificNode(toolSettings, node);
                if (!String.IsNullOrEmpty(errMsg))
                {
                    return errMsg;
                }
            }

            return errMsg;
        }

        private string WriteNodeSpecificFilesForSpecificNode(ToolSettings toolSettings, NodeSpecificSetting nodeSpecificSetting)
        {
            string errMsg = "";

            // Copy Web API setting(s)
            string webApiAppSettingsSourcePath = Path.Combine(toolSettings.PDMPWebApiSourcePath, "appsettings.json");
            string webApiAppsettingsDestinationPath = Path.Combine(toolSettings.DeploymentPath, "Nodes", nodeSpecificSetting.NodeName, WEB_API_NODE_FOLDER_NAME, "appsettings.json");
            errMsg = WriteWebApiAppSettingsFile(webApiAppSettingsSourcePath, webApiAppsettingsDestinationPath, nodeSpecificSetting);
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = $"Error writing Web API appsetting.json for node: {nodeSpecificSetting.NodeName}\r\n" + errMsg;
                return errMsg;
            }
            // Copy Web API setting(s)

            // Copy ads-sync-framwork setting(s)
            string adsSyncFrameworkAppConfigSourcePath = Path.Combine(toolSettings.AdsSyncFrameworkSourcePath, "ads-sync-framework.exe.config");
            string adsSyncFrameworkAppConfigDestinationPath = Path.Combine(toolSettings.DeploymentPath, "Nodes", nodeSpecificSetting.NodeName, ADS_SYNC_FRAMEWORK_NODE_FOLDER_NAME, "ads-sync-framework.exe.config");
            errMsg = WriteAdsSyncFrameworkAppConfigFile(adsSyncFrameworkAppConfigSourcePath, adsSyncFrameworkAppConfigDestinationPath, nodeSpecificSetting);
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = $"Error writing ads-sync-framework.exe.config for node: {nodeSpecificSetting.NodeName}\r\n" + errMsg;
                return errMsg;
            }
            // Copy ads-sync-framwork setting(s)

            // Copy Change Management Tools setting(s)
            string changeManagementToolsAppSettingsSourcePath = Path.Combine(toolSettings.ChangeManagementLogParserSourcePath, "appsettings.json");
            string changeManagementToolsAppSettingsDestinationPath = Path.Combine(toolSettings.DeploymentPath, "Nodes", nodeSpecificSetting.NodeName, CHANGE_MANAGEMENT_TOOLS_COMMON_FOLDER_NAME, "appsettings.json");
            errMsg = WriteChangeManagementToolsSettingsFile(changeManagementToolsAppSettingsSourcePath, changeManagementToolsAppSettingsDestinationPath, nodeSpecificSetting);
            if (!String.IsNullOrEmpty(errMsg))
            {
                errMsg = $"Error writing Change Management Tools appsetting.json for node: {nodeSpecificSetting.NodeName}\r\n" + errMsg;
                return errMsg;
            }
            // Copy Change Management Tools setting(s)

            return errMsg;
        }

        private string WriteWebApiAppSettingsFile(string pathToSourceAppSettingsFile, string pathToDestinationAppSettingsFile, NodeSpecificSetting nodeSpecificSetting)
        {
            try
            {
                string fileContent = File.ReadAllText(pathToSourceAppSettingsFile);

                dynamic appsettings = JObject.Parse(fileContent);

                appsettings.AzureServiceBus.keyname = GetEmptyStringIfNull(nodeSpecificSetting.AzureServiceBus_keyName);
                appsettings.Database.ConnectionString = GetEmptyStringIfNull(nodeSpecificSetting.Database_ConnectionString);
                appsettings.Database.TransmitConnectionString = GetEmptyStringIfNull(nodeSpecificSetting.Database_TransmitConnectionString);
                appsettings.Clarity.baseurl = GetEmptyStringIfNull(nodeSpecificSetting.Clarity_BaseUrl);
                appsettings.Clarity.restkeyname = GetEmptyStringIfNull(nodeSpecificSetting.Clarity_RestKeyName);
                appsettings.URL = GetEmptyStringIfNull(nodeSpecificSetting.NodeURL);
                appsettings.Maintenance.SenderEmailAddress = GetEmptyStringIfNull(nodeSpecificSetting.Maintenance_SenderEmailAddress);
                appsettings.Maintenance.ReceiverEmailAddress = GetEmptyStringIfNull(nodeSpecificSetting.Maintenance_ReceiverEmailAddress);
                appsettings.StartupCheck.NodeStartupPathCheck = GetEmptyStringIfNull(nodeSpecificSetting.StartupCheck_NodeStartupPathCheck);
                appsettings.ChangeManagement.ConnectionStringKeyName = GetEmptyStringIfNull(nodeSpecificSetting.ChangeManagement_ConnectionStringKeyName);
                appsettings.ChangeManagement.BlobContainerName = GetEmptyStringIfNull(nodeSpecificSetting.ChangeManagement_BlobContainerName);
                appsettings.TransmitProjectBackup.ConnectionStringKeyName = GetEmptyStringIfNull(nodeSpecificSetting.TransmitProjectBackup_ConnectionStringKeyName);
                appsettings.TransmitProjectBackup.BlobContainerName = GetEmptyStringIfNull(nodeSpecificSetting.TransmitProjectBackup_BlobContainerName);
                appsettings.TransmitIssueLog.ConnectionStringKeyName = GetEmptyStringIfNull(nodeSpecificSetting.TransmitIssueLog_ConnectionStringKeyName);
                appsettings.TransmitIssueLog.BlobContainerName = GetEmptyStringIfNull(nodeSpecificSetting.TransmitIssueLog_BlobContainerName);
                
                string newFileContent = JsonConvert.SerializeObject(appsettings, Formatting.Indented);

                File.WriteAllText(pathToDestinationAppSettingsFile, newFileContent);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            return "";
        }

        private string WriteAdsSyncFrameworkAppConfigFile(string sourceAppConfigFilePath, string destinationAppConfigFilePath, NodeSpecificSetting nodeSpecificSetting)
        {
            string errMsg = "";

            try
            {
                File.Copy(sourceAppConfigFilePath, destinationAppConfigFilePath, true);

                ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap()
                {
                    ExeConfigFilename = destinationAppConfigFilePath
                };

                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap, ConfigurationUserLevel.None);

                config.AppSettings.Settings["dbConnectionString"].Value = GetEmptyStringIfNull(nodeSpecificSetting.Database_ConnectionString);
                config.AppSettings.Settings["ControllerApiRoot"].Value = GetEmptyStringIfNull(nodeSpecificSetting.NodeURL);
                config.AppSettings.Settings["Region"].Value = GetEmptyStringIfNull(nodeSpecificSetting.AdsSyncFrameworkRegion);
                config.AppSettings.Settings["AdsBaseUrl"].Value = GetEmptyStringIfNull(nodeSpecificSetting.AdsBaseUrl);
                config.AppSettings.Settings["AdsSubscriptionValue"].Value = GetEmptyStringIfNull(nodeSpecificSetting.AdsSubscriptionValue);
                config.AppSettings.Settings["keyvault"].Value = GetEmptyStringIfNull(nodeSpecificSetting.KeyVault);
                config.AppSettings.Settings["keyvaultclientid"].Value = GetEmptyStringIfNull(nodeSpecificSetting.KeyVaultClientId);
                config.AppSettings.Settings["keyvaulttenantid"].Value = GetEmptyStringIfNull(nodeSpecificSetting.KeyVaultTenantId);
                config.AppSettings.Settings["ADSkeyvaultKeyname"].Value = GetEmptyStringIfNull(nodeSpecificSetting.AdsKeyVaultKeyName);
                config.AppSettings.Settings["WinCredClientSecret"].Value = GetEmptyStringIfNull(nodeSpecificSetting.WinCredClientSecret);
                config.AppSettings.Settings["OtherNodes"].Value = GetEmptyStringIfNull(nodeSpecificSetting.OtherNodes);

                config.Save(ConfigurationSaveMode.Modified);

                errMsg = RemoveAutomaticallyGeneratedDataFromAppConfigFile(destinationAppConfigFilePath);
            }
            catch (Exception e)
            {
                errMsg = e.ToString();
            }

            return errMsg;
        }

        private string WriteChangeManagementToolsSettingsFile(string pathToSourceAppSettingsFile, string pathToDestinationAppSettingsFile, NodeSpecificSetting nodeSpecificSetting)
        {
            try
            {
                string fileContent = File.ReadAllText(pathToSourceAppSettingsFile);

                dynamic appsettings = JObject.Parse(fileContent);

                appsettings.Azure.Storage.ConnectionStringKeyName = GetEmptyStringIfNull(nodeSpecificSetting.ChangeManagement_ConnectionStringKeyName);
                appsettings.Azure.Storage.BlobContainerName = GetEmptyStringIfNull(nodeSpecificSetting.ChangeManagement_BlobContainerName);
                appsettings.Regional.Region = GetEmptyStringIfNull(nodeSpecificSetting.ChangeManagementToolsRegion);
                appsettings.Regional.NodeURL = GetEmptyStringIfNull(nodeSpecificSetting.NodeURL);

                string newFileContent = JsonConvert.SerializeObject(appsettings, Formatting.Indented);

                File.WriteAllText(pathToDestinationAppSettingsFile, newFileContent);
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            return "";
        }

        private string RemoveAutomaticallyGeneratedDataFromAppConfigFile(string path)
        {
            string errMsg = "";

            try
            {
                List<string> correctedLines = new List<string>();

                foreach (var line in File.ReadAllLines(path))
                {
                    if (line.Contains("appSettings") && line.Contains("file=\"\""))
                    {
                        correctedLines.Add(line.Replace(" file=\"\"", ""));
                    }
                    else
                    {
                        if (!line.Contains("<clear />"))
                        {
                            correctedLines.Add(line);
                        }
                    }
                }

                File.WriteAllLines(path, correctedLines);
            }
            catch (Exception e)
            {
                errMsg = e.ToString();
            }

            return "";
        }

        private string GetEmptyStringIfNull(string s)
        {
            if (String.IsNullOrWhiteSpace(s))
            {
                return String.Empty;
            }
            else
            {
                return s;
            }
        }
    }
}
