﻿using PDEDeploymentTool.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;

namespace PDEDeploymentTool.Logic
{
    public class ToolHelper
    {
        private static readonly string NODE_SPECIFIC_SETTINGS_FILE_NAME = "NodeSpecificSettings.txt";

        public static string ShowFolderBrowserDialog()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }
            else
            {
                return "";
            }
        }

        public static ToolSettings ReadToolSettings()
        {
            ToolSettings toolSettings = new ToolSettings();
            Type type = typeof(ToolSettings);
            string propertyName;
            string propertyValue;
            foreach (PropertyInfo property in type.GetProperties())
            {
                propertyName = property.Name;
                propertyValue = ConfigurationManager.AppSettings[propertyName];
                property.SetValue(toolSettings, propertyValue);
            }
            return toolSettings;
        }

        public static void WriteToolSettings(ToolSettings toolSettings)
        {
            try
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                Type type = typeof(ToolSettings);
                string propertyName;
                string propertyValue;
                foreach (PropertyInfo property in type.GetProperties())
                {
                    propertyName = property.Name;
                    propertyValue = property.GetValue(toolSettings)?.ToString();
                    if (config.AppSettings.Settings.AllKeys.Any(k => k == propertyName))
                    {
                        config.AppSettings.Settings[propertyName].Value = propertyValue;
                    }
                    else
                    {
                        config.AppSettings.Settings.Add(propertyName, propertyValue);
                    }
                }
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                System.Windows.MessageBox.Show("Configurations have been saved, successfully", "Tool Configuration Saving", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show($"Error saving configurations:\r\n{e}", "Tool Configuration Saving", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public static List<NodeSpecificSetting> ReadNodeSpecificSettings()
        {
            List<NodeSpecificSetting> nodeSpecificSettings = new List<NodeSpecificSetting>();
            NodeSpecificSetting nodeSpecificSetting = null;

            if (File.Exists(NODE_SPECIFIC_SETTINGS_FILE_NAME))
            {
                string[] allLines = File.ReadAllLines(NODE_SPECIFIC_SETTINGS_FILE_NAME);
                foreach (var line in allLines)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    nodeSpecificSetting = NodeSpecificSetting.FromJson(line);
                    if (nodeSpecificSetting != null)
                    {
                        nodeSpecificSettings.Add(nodeSpecificSetting);
                    }
                }
            }

            return nodeSpecificSettings;
        }

        public static void WriteNodeSpecificSettings(List<NodeSpecificSetting> nodeSpecificSettings)
        {
            if (nodeSpecificSettings == null || nodeSpecificSettings.Count == 0)
            {
                return;
            }

            List<string> jsonStrings = new List<string>();
            string jsonString = "";

            foreach (var nodeSpecificSetting in nodeSpecificSettings)
            {
                jsonString = NodeSpecificSetting.ToJson(nodeSpecificSetting);
                if (!String.IsNullOrEmpty(jsonString))
                {
                    jsonStrings.Add(jsonString);
                }
            }

            if (jsonStrings.Count == 0)
            {
                return;
            }

            try
            {
                File.WriteAllText(NODE_SPECIFIC_SETTINGS_FILE_NAME, String.Join("\r\n", jsonStrings));
                System.Windows.MessageBox.Show("All configurations have been saved, successfully", "Saving Configurations", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception e)
            {
                using (StreamWriter sw = File.AppendText("Log.txt"))
                {
                    sw.WriteLine($"Error writing the string below to {NODE_SPECIFIC_SETTINGS_FILE_NAME}:");
                    sw.WriteLine($"{String.Join("\r\n", jsonStrings)}");
                    sw.WriteLine("Exception:");
                    sw.WriteLine($"{e.ToString()}");
                    sw.WriteLine("==================================================");
                }
            }
        }
    }
}
